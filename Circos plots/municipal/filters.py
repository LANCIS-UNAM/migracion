import municipios


def migration(l):
    #ID_VIV,ENT,NOM_ENT,MUN,NOM_MUN,FACTOR,SEXO,EDAD,MUN_RES10,NOM_MUN_RES10,ENT_PAIS_RES10,CONACT,migrantes4mun
    #7,Chiapas,

    if l['NOM_MUN_RES10'] not in municipios.abreviaturas:
        if l['ENT_PAIS_RES10'] and int(float(l['ENT_PAIS_RES10'])) != 7:
            return True
        else:
            return False
    
    if int(l['MUN']) != int(float(l['MUN_RES10'])):     # must change municipality
        return True
    else:
        return False



# must rescale output regions in contigs, so grab links file
# contigs = {ent: []
#            for ent in estados.abreviaturas.values() if ent != 'ext'}

# with open('links.txt') as f:
#     for l in f.readlines():
#         # AGU	0	520	BCN	0	520	color=JimmyRed1_a3
#         ent = l.split()[0]

#         contigs[ent].append(int(l.split()[1]))
#         contigs[ent].append(int(l.split()[2]))

