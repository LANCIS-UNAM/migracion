import csv
from itertools import permutations

import estados
from filters import interstate_migration, contigs


sex = {ent: {'1': 0, '2': 0, '3': 0}
       for ent in estados.abreviaturas.values()
       if ent != 'ext'}

#ID_VIV,ENT,NOM_ENT,MUN,NOM_MUN,FACTOR,SEXO,EDAD,MUN_RES10,NOM_MUN_RES10,ENT_PAIS_RES10,CONACT,migrantes4mun

with open('../migrantes_int_2010_2015.csv') as f:
    reader = csv.DictReader(f)

    for l in reader:
        if interstate_migration(l):
            sex[estados.abreviaturas[l['NOM_ENT']]][l['SEXO']] += int(l['FACTOR'])


with open('sex.txt', 'w', newline='\n') as f:
    writer = csv.writer(f, delimiter='\t')

    for ent in sex:

        N = sum(sex[ent].values())

        offset = min(contigs[ent])
        for d in ['1', '2', '3']:
            if sex[ent][d] > 0:

                max_offset = max(contigs[ent]) - min(contigs[ent])
                scaled = int(sex[ent][d] * max_offset / float(N))

                writer.writerow([ent,
                                 str(offset),
                                 str(offset + scaled),
                                 d])
                offset += scaled
