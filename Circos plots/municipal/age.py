import csv
from itertools import permutations

import estados
from filters import interstate_migration, contigs


age_bins = ['0-5', '6-14', '15-29', '30-59', '60+']
age = {ent: {bin_: 0
             for bin_ in age_bins}
       for ent in estados.abreviaturas.values()}
del(age['ext'])

def get_bin(edad):
    if int(edad) <= 5:
        return '0-5'
    elif int(edad) <= 14:
        return '6-14'
    elif int(edad) <= 29:
        return '15-29'
    elif int(edad) <= 59:
        return '30-59'
    elif int(edad) >= 60:
        return '60+'


with open('../migrantes_int_2010_2015.csv') as f:
    reader = csv.DictReader(f)

    for l in reader:
        if interstate_migration(l):
            age[estados.abreviaturas[l['NOM_ENT']]][get_bin(l['EDAD'])] += int(l['FACTOR'])


with open('age.txt', 'w', newline='\n') as f:
    writer = csv.writer(f, delimiter='\t')

    for ent in age:

        N = sum(age[ent].values())
        
        offset = min(contigs[ent])
        for a in age_bins:
            max_offset = max(contigs[ent]) - min(contigs[ent])
            scaled = int(age[ent][a] * max_offset / float(N))

            writer.writerow([ent,
                             str(offset),
                             str(offset + scaled),
                             age_bins.index(a) + 1])
            offset += scaled
