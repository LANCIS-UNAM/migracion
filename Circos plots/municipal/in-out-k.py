import networkx as nx
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
from math import log10

G = nx.DiGraph()

with open('links.txt') as f:
    for l in f.readlines():
        link = l.split()
        s = link[0]
        t = link[3]

        W = int(link[2]) - int(link[1])

        w = G.get_edge_data(s, t, {'w': 0})['w']
        
        w += W
        
        G.add_edge(s, t, w=w)


ins = [log10(G.in_degree(n, weight='w'))
       for n in G.nodes]

outs = []
for n in G.nodes:
    k = G.out_degree(n, weight='w')
    if k > 0:
        outs.append(log10(k))
    else:
        outs.append(k)


fig = plt.figure(dpi=333)
plt.scatter(ins, outs, marker='.', color='#763a4e', linewidth=0)

ax = fig.gca()
ax.set_aspect('equal')
ax.set_xlim(-0.2, 5.5)
ax.set_ylim(-0.2, 5.5)
ax.set_xlabel('in degree')
ax.set_ylabel('out degree')

# diagonal
line = mlines.Line2D([0, 1], [0, 1], color='#607e91', linewidth=0.2)
transform = ax.transAxes
line.set_transform(transform)
ax.add_line(line)


plt.savefig('in-out-scatter.png')
