import csv
from itertools import permutations

import municipios as muns
from filters import migration # , contigs


edges = {e: 0
         for e in permutations(list(muns.abreviaturas.values()), 2)}  # weighted edges


with open('../migrantes_chiapas.csv') as f:
    reader = csv.DictReader(f)

    for l in reader:
        if migration(l):
            s = muns.abreviaturas[l['NOM_MUN']]
            if int(float(l['ENT_PAIS_RES10'])) != 7:
                t = 'ext'
            else:
                t = muns.abreviaturas[l['NOM_MUN_RES10']]

            edges[(s, t)] += int(l['FACTOR'])



offset = {mun: 0
          for mun in muns.abreviaturas.values()}

with open('links.txt', 'w') as f:
    writer = csv.writer(f, delimiter='\t')

    for e in edges:
        if edges[e] == 0:
            continue

        s, t = e
        #ent1 0 1402 ent32 0 1402 color=oranges-13-seq-1
        if s in ('19', '41', '52', '99'):
            muncolor = 'red'
        else:
            muncolor = muns.color[s] + "_a3"

        writer.writerow(
            [s,
             offset[s],
             offset[s] + edges[e],
             t,
             offset[t],
             offset[t] + edges[e],
             'color=' + muncolor])

        offset[s] += edges[e]
        offset[t] += edges[e]


with open('karyotype.txt', 'w') as f:
    writer = csv.writer(f, delimiter='\t')
    for mun in offset:
        if offset[mun] > 0:
            # chr - ent1 ags	0 1402 oranges-13-seq-1
            if mun in ('19', '41', '52', '99'):
                muncolor = 'red'
            else:
                muncolor = muns.color[mun] + "_a3"
            writer.writerow(
                ["chr",
                 "-",
                 mun,
                 mun,
                 0,
                 offset[mun],
                 muncolor])
