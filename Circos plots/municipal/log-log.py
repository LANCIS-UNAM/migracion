import networkx as nx
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
from matplotlib.ticker import FormatStrFormatter
from math import log

G = nx.DiGraph()

with open('links.txt') as f:
    for l in f.readlines():
        link = l.split()
        s = link[0]
        t = link[3]
        W = int(link[2]) - int(link[1])

        w = G.get_edge_data(s, t, {'w': 0})['w']
        
        w += W
        
        G.add_edge(s, t, w=w)


ins = [G.in_degree(n, weight='w') / 1000.0
       for n in G.nodes]

outs = []
for n in G.nodes:
    k = G.out_degree(n, weight='w') 
    if k > 0:
        outs.append(k)
        
k = [G.degree(n, weight='w')
     for n in G.nodes]

        
# plt.scatter([log(i) for i in sorted(ins, reverse=True)], range(len(ins)), color='#eb3649')
# plt.scatter([log(i) for i in sorted(outs, reverse=True)], range(len(outs)), color='#346645')
# plt.show()


outs = [G.out_degree(n, weight='w') / 1000.0
        for n in G.nodes]

plt.subplots(figsize=(10, 7))
plt.bar(range(len(ins)), sorted(ins, reverse=True), 0.5, color='#e8be70')
plt.bar([i+0.5 for i in range(len(outs))], sorted(outs, reverse=True), 0.5, color='#77002a')


plt.tick_params(
    axis='x',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    bottom=False,      # ticks along the bottom edge are off
    top=False,         # ticks along the top edge are off
    labelbottom=False) # labels along the bottom edge are off

plt.savefig('histogram.png')
