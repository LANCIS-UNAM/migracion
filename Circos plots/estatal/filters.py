import estados


def interstate_migration(l):
    if l['ENT_PAIS_RES10']:
        target = int(float(l['ENT_PAIS_RES10']))
    else:
        return False

    if target and int(l['ENT']) != target:  # no self loops
        if target == 999 or target <= 32:
            return True

    return False



# must rescale output regions in contigs, so grab links file
contigs = {ent: []
           for ent in estados.abreviaturas.values() if ent != 'ext'}

with open('links.txt') as f:
    for l in f.readlines():
        # AGU	0	520	BCN	0	520	color=JimmyRed1_a3
        ent = l.split()[0]

        contigs[ent].append(int(l.split()[1]))
        contigs[ent].append(int(l.split()[2]))

