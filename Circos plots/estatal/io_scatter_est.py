import networkx as nx
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
from matplotlib.ticker import FormatStrFormatter

G = nx.DiGraph()

with open('links.txt') as f:
    for l in f.readlines():
        link = l.split()
        s = link[0]
        t = link[3]
        W = int(link[2]) - int(link[1])

        w = G.get_edge_data(s, t, {'w': 0})['w']
        
        w += W
        
        G.add_edge(s, t, w=w)


ins = [G.in_degree(n, weight='w')/1000
       for n in G.nodes]

outs = [G.out_degree(n, weight='w')/1000
        for n in G.nodes]


fig = plt.figure(dpi=333)
plt.scatter(ins, outs, marker='o', color='#d4af55', linewidth=0)

ax = fig.gca()
ax.set_aspect('equal')
ax.set_xlim(0, 900)
ax.set_ylim(0, 900)
ax.set_xlabel('in degree')
ax.set_ylabel('out degree')
ax.xaxis.set_major_formatter(FormatStrFormatter('%dk'))
ax.yaxis.set_major_formatter(FormatStrFormatter('%dk'))

# diagonal
line = mlines.Line2D([0, 1], [0, 1], color='#607e91', linewidth=0.4)
transform = ax.transAxes
line.set_transform(transform)
ax.add_line(line)


plt.savefig('in-out-scatter.png')

