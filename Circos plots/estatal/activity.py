import csv
from itertools import permutations

import estados
from filters import interstate_migration, contigs

act_bins = ['ocupada', 'desocupada', 'inactiva', 'no especificada']
def get_bin(act):
    act = int(act)
    if act >= 10 and act <= 16:
        return 'ocupada'
    elif act == 20:
        return 'desocupada'
    elif act >= 31 and act <= 35:
        return 'inactiva'
    elif act == 99:
        return 'no especificada'



act = {ent: {bin_: 0
             for bin_ in act_bins}
       for ent in estados.abreviaturas.values()
       if ent != 'ext'}


with open('../migrantes_int_2010_2015.csv') as f:
    reader = csv.DictReader(f)

    for l in reader:
        if interstate_migration(l):
            if l['CONACT'] == '':
                continue
            else:
                conact = int(float(l['CONACT']))
            act[estados.abreviaturas[l['NOM_ENT']]][get_bin(conact)] += int(l['FACTOR'])


with open('activity.txt', 'w', newline='\n') as f:
    writer = csv.writer(f, delimiter='\t')

    for ent in act:

        N = sum(act[ent].values())

        if N == 0:
            print(ent, age[ent])
            exit()
        
        offset = min(contigs[ent])
        for a in act_bins:

            max_offset = max(contigs[ent]) - min(contigs[ent])            
            scaled = int(act[ent][a] * max_offset / float(N))

            writer.writerow([ent,
                             str(offset),
                             str(offset + scaled),
                             act_bins.index(a)])
            offset += scaled
