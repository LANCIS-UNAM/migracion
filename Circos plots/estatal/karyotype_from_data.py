import csv
import estados

contigs = {ent: 0
           for ent in estados.abreviaturas.values()}

del(contigs['ext'])

with open('../migrantes_int_2010_2015.csv') as f:
    reader = csv.DictReader(f)

    for l in reader:
        if l['ENT_PAIS_RES10']:
            target = int(float(l['ENT_PAIS_RES10']))
        else:
            target = None

        if target and int(l['ENT']) != target:  # no self loops
            if target == 999 or target <= 32:
                contigs[estados.abreviaturas[l['NOM_ENT']]] += int(l['FACTOR'])


with open('karyotype.txt', 'w') as f:
    for ent in contigs:
        # chr - ent1 ags	0 1402 oranges-13-seq-1    
        f.write("chr\t-\t%s\t%s\t0\t%s\t%s\n" % (ent, ent, contigs[ent], estados.color[ent]))

    f.write("chr	-	ext	ext	0	864767	white")


        
