import csv
import estados
from itertools import permutations

edges = {e: 0
         for e in permutations(list(estados.abreviaturas.values()) + ['ext', ], 2)}  # weighted edges

offset = {ent: 0
          for ent in estados.abreviaturas.values()}
offset['ext'] = 0


#ID_VIV,ENT,NOM_ENT,MUN,NOM_MUN,FACTOR,SEXO,EDAD,MUN_RES10,NOM_MUN_RES10,ENT_PAIS_RES10,CONACT,migrantes4mun

with open('../migrantes_int_2010_2015.csv') as f:
    reader = csv.DictReader(f)

    for l in reader:
        if l['ENT_PAIS_RES10']:
            target = int(float(l['ENT_PAIS_RES10']))
        else:
            target = None

        if target and int(l['ENT']) != target:  # no self loops
            if target == 999 or target <= 32:
                edges[(estados.abreviaturas[l['NOM_ENT']],
                       estados.by_id(target))] += int(l['FACTOR'])


with open('links.txt', 'w') as f:
    for e in edges:
        if edges[e] == 0:
            continue

        s, t = e
        #ent1 0 1402 ent32 0 1402 color=oranges-13-seq-1
        f.write("\t".join(
            [str(i)
             for i in [s,
                       offset[s],
                       offset[s] + edges[e],
                       t,
                       offset[t],
                       offset[t] + edges[e],
                       'color=' + estados.color[s] + "_a3"]]) + "\n")

        offset[s] += edges[e]
        offset[t] += edges[e]

with open('karyotype.txt', 'w') as f:
    for ent in offset:
        # chr - ent1 ags	0 1402 oranges-13-seq-1    
        f.write("chr\t-\t%s\t%s\t0\t%s\t%s_a3\n" % (ent, ent, offset[ent], estados.color[ent]))
