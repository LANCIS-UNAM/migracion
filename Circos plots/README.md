# Migration in Chiapas and other states in Mexico

We have created circos plots because of their high ratio of
information to ink. We have managed to display most of the data for
each set, which in turn allows for a system-wide analysis.

Drilling down from this overview we created scatterplots to analyze
the ratio of emigration to inmigration, and to reveal patterns in the
frequency of migration per region.

## Inter-state migration

![estatal](estatal/circos.png)


From outermost to innermost:
  1. occupation categories: ocupado, desocupado, inactivo, no especificado (Painted2, Painted3, Painted4, Painted5)
  2. age range, from lightest to darkest: 0-5; 6-14; 15-29; 30-59; 60+
  3. sex, darker means male, lighter female
  4. ribbon colors match circunference colors to distinguish source and target

A few states seem to have the same rates of emigration as
immigration. Most have more emigrants than immigrants. This
observation is reinforced by the scatter plots of in-out degree below.

Taken together, migration to and from EdoMex and CDMX is easily an
order of magnitude larger than any other state in the country.

Considering them together makes sense since the Metropolitan Area of
the Valley of Mexico is a megalopolis made up of municipalities from
both.

The distribution of ribbon widths suggests a power-law
distribution. It seems that there are very few very large ones, a
large quantity of medium ones, and a very large rest of very small
ones.

This histogram has the shape of a textbook power-law:

![estatal](estatal/histogram.png)

Yellow bars are inbound migration, red are outbound.

A power-law distribution of connectivity suggests a scale-free
phenomenon, and self-preferential attachment. I.e. from any given
municipality there are other municipalities which seem more atractive,
to which people wish to migrate. This is the case even while living in
the Megalopolis, considering external migration: most emigrants from
that region are external migration.

Most notorious emigration from Chiapas is
threeways: 1. external 2. the megalopolis 3. Oaxaca. People from
Chiapas move to other municipalities, but these movements are not as
clear in the big picture.

The largest occupation bins are "ocupado" and "inactivo", which are
roughly econommically active and inactive. This suggests that close to
half of the people move because they are econommically inactive. It is
striking that the other half is actually active. It would seem that
beeing economically active does not amount to being secure in any
given municipality. Migrating while economicaly active is thus a
diversification strategy, whereas migration while not active seems
like a coping strategy.

Sex seems to be evenly distributed accross all states. This is a
little surprising, one would think that considering the empirical
evidence on mexican migration (**see references**) involved it would
be a mostly male activity.

Less surprising is the proportions of old and young people moving,
although their numbers are not small.  Most people are in the second
and third bins, i.e. ages from 15 to 59.


## Inter-municipality migration in Chiapas

![municipal](municipal/circos.png)

Labels are municipal codes. Ticks labels represent thousands.

Within Chiapas the distribution of ribbon widths is similar to that of
nation-wide migration. There seem to be four municipalities which
account for most of the migration. Then a dozen of medium sized nodes,
and a long tail of small nodes. 

![estatal](municipal/histogram.png)

Yellow bars are inbound migration, red are outbound.

A power-law distribution also suggests a scale-free phenomenon, and
the same self-preferential attachment described above: from any
municipality there are others which seem better places to be in.

Geography might explain why most people moving away from 27 go to 101.

101 is sure to be the capital city (indeed 101 is Tuxtla
Gutiérrez). The width therefore could be explained due to political
and economical reasons. 89 is Tapachula, a municipality strongly
linked with migration flows particularly from Central America.

People from 19, 41, 52, and 99 do not seem to prefer any destination
when emigrating, one can see ribbons going from these municipalities
to all other. One common characteristic (19 a little less) is: most
movement is emigration.

It seems like the smaller the municipality, the higher ratio of
emigration. Maybe do the numbers too.



## In/out degree

These scatterplots we did by connecting states and municipalities as
nodes in a graph, then computing their in and out degrees.

Points are states and municipalities, respectively


## Inter-state
![estatal](estatal/in-out-scatter.png)

Two dots are far to the right, both over and under the diagonal. These
dots are sure to be CDMX and EdoMex, and it is striking that they are
at the same time the places that most recieve immigrants and send
emigrants.

The furthest dot to the right is Exterior, which is why it has zero
out-degree, no one emigrates from Exterior in our dataset.

As for the rest of the dots, most are on the top left side which means
that from most states there are more people moving out than in. 

Yet they are very close to the diagonal, which means they emit
emigrants almost as much as they host immigrants. Three or four dots
seem to be close to vertical, these are places from which people
emigrate more than immigrate. It would be interesting to place another
diagonal, and to seek those places in the data and try to understand
what is going on. I suspect those dots are northern municipalities,
places near the border with the U.S.

A diagonal on the top left would create two categories of
municipalities. Yet a diagonal on the bottom right would not cut
accross any cluster.


## Inter-municipality (log10/log10)
![municipal](municipal/in-out-scatter.png)

Logarithms are used on both dimensions because the differences in
scale between small and large municipalities made it impossible to
consider all points in the same graph otherwise.

But once they are brought to the same logarithmic scale, it is quite
notorious that most municipalities emit people, while just a few are
hosts for immigrants. There are considerably more dots on the top left
than on the bottom right.

The furthest dot to the right is Exterior, which is why it has zero
out-degree, no one emigrates from Exterior in our dataset.
