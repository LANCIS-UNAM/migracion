from colour import Color

colors = {
    "Anthocyanins1": ["#f8f1d9", "#edc262", "#9a3a28", "#581d2c", "#2e1342"],
    "Anthocyanins2": ["#f4e8a7", "#e7a96f", "#b05732", "#6a3131", "#2f2929"],
    "RubyGold": ["#57170f", "#943124", "#eabf5a", "#f4e093", "#edaa5d", "#e77e39"],
    "Sweetest": ["#b2c66b", "#696d2e", "#f9f2b2", "#f1cf62", "#6f4d22"],
    "GlassGem": ["#d1aae6", "#6c5ba5", "#e66e58", "#7f346f", "#9d2315"],
    "PodCorn": ["#b28044", "#debc80", "#f1dfda", "#dac9ae", "#988e79"],
    "HighlandMAGIC": ["#d73c6c", "#db86a5", "#f2cb5d", "#faebb6", "#914d8f", "#4b1f96"],
    "MaizAzul": ["#160d23", "#6e79a2", "#acb1d1", "#e2e7e9", "#9b969c", "#384073"],
    "JimmyRed": ["#61241c", "#b66859", "#d59578", "#f1deda", "#ddbb7f", "#a04230"],
    "FloweringTime": ["#933141", "#d1aac5", "#f1e89f", "#c1cb68", "#7c7c46", "#626d5c"],
    "HopiBlue": ["#763a4e", "#a192ac", "#fcf3ba", "#d4af55", "#607e91", "#28344f"],
    "Painted": ["#e77e39", "#9e5727", "#efc164", "#a27937", "#e66e58", "#9d462f"],
    "MaizMorado": ["#100708", "#6F0E2E", "#BC184E", "#D77495", "#E4B9C7"],
    "OaxacaGreen": ["#CAC67A", "#426F4D", "#213827", "#2C5273", "#817A35"], }



with open('colors.maize.conf', 'w') as f:
    for name in colors:
        for n, hexcolor in enumerate(colors[name]):
            color = Color(hexcolor)

            f.write(name + str(n) + "=" +
                    ",".join([str(int(v * 255))
                              for v in color.get_rgb()])
                    + "\n")
