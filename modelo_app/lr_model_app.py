import streamlit as st
import pandas as pd
import shap
import matplotlib.pyplot as plt
import numpy as np
import pickle

st.write("""
# Model of climate change-related internal migration in Mexico 

This is an interactive tool that follows the methods and data described in: **Mejía-ciro, J. D., García-meneses, P. M., Ramírez, G. C., Salgado Nieto, U., Aragonés Castañer, A. M., Mazari-Hiriart, M., & García-herrera, R. (2021). The analysis of the causes of climate change related internal migration of rural households in Chiapas, Mexico: A quali-quantitative approach. *Frontiers in Human Dynamics*, 1–31.**

- The app shows the effect of specific variables on the probability of internal environmental migration over a period of five years (2010-2015). A logistic regression model was constructed studying the effect of the **drivers of environmental migration** (Black et el.,2011) together with bioclimatic indices. We used the **sustainable livelihoods framework** (Ashley & Carney, 1999; Binder et al., 2013; Scoones, 1998), to *operationalize* the drivers of environmental migration. 

How to use:

1. Specify a value for all variables in the *sidebar* on the left, according to the livelihood capitals of a mexican household you wish to analyze.
2. Variables 1 to 22 are at household level, variables 23 to 27 are at the household´s municipality level.
3. The outcome shows the household´s probability of having at least one internal migrant given certain specified values for the variables which are also known as conditional probabilities.

""")
st.write('---')

# Sidebar
# Header of Specify Input Parameters
st.sidebar.header('Specify livelihood capitals for a household')
st.sidebar.subheader('Default values correspond to mean values in EIC2015 (INEGI, 2015) undersampled dataset.')

#intercept = st.sidebar.slider('intercept', 0.000, 1.000, 0.943)
def user_input_features():
    COCINA_1 = st.sidebar.slider('1. household has kitchen?', 0.00, 1.00, 0.20)
    COMBUSTIBLE_1 = st.sidebar.slider('2. kitchen requires wood or coal to cook?', 0.00, 1.00, 0.31)
    ELECTRICIDAD_5 = st.sidebar.slider('3. household has electricity?', 0.00, 1.00, 0.97)
    ABA_AGUA_ENTU_1 = st.sidebar.slider('4. household has piped water?', 0.00, 1.00, 0.75)
    AUTOPROP_7 = st.sidebar.slider('5. family owns a car?', 0.00, 1.00, 0.35)
    CELULAR_3 = st.sidebar.slider('6. family owns a mobile phone?', 0.00, 1.00, 0.69)
    TENENCIA_1 = st.sidebar.slider('7. Owner lives in the household?', 0.00, 1.00, 0.70)
    TENENCIA_2 = st.sidebar.slider('8. household rented?', 0.00, 1.00, 0.14)
    DEUDA_1 = st.sidebar.slider('9. household is free of debt?', 0.00, 1.00, 0.03)
    DEUDA_2 = st.sidebar.slider('10. household debt is being paid?', 0.00, 1.00, 0.06)
    INGR_PEROTROPAIS_1 = st.sidebar.slider('11. household remittances from people outside Mexico?', 0.00, 1.00, 0.07)
    INGR_PERDENTPAIS_3 = st.sidebar.slider('12. household income from people inside Mexico?', 0.00, 1.00, 0.08)
    INGR_AYUGOB_5 = st.sidebar.slider('13. household income from government?', 0.00, 1.00, 0.39)
    TERRENO_AGROPE_1 = st.sidebar.slider('14. Any family member owns a property that is farmed?', 0.00, 1.00, 0.16)
    CONACT_10 = st.sidebar.slider('15. Number of people that worked as an employee/ laborer', 0.00, 28.00, 1.26)
    CONACT_13 = st.sidebar.slider('16. Number of people that worked as an agriculture laborer', 0.00, 7.00, 0.02)
    CONACT_31 = st.sidebar.slider('17. Number of people that studied a week before the survey', 0.00, 18.00, 0.44)
    INSAN = st.sidebar.slider('18. Food insecurity levels (EMSA); security=0; Mild= 1; Moderate = 2; Severe= 3)', 0.00, 3.00, 0.66)
    ALFABET_5_J = st.sidebar.slider('19. household head level knows how to read and write?', 0.00, 1.00, 0.87)
    SERSALUD_NO = st.sidebar.slider('20. household has no access to health service?', 0.00, 18.00, 0.07)
    EDAD_0_5 = st.sidebar.slider('21. Number of people of ages 0 to 5 per household', 0.00, 9.00, 0.39)
    EDAD_30_59 = st.sidebar.slider('22. Number of people of ages 30 to 59 per household', 0.00, 11.00, 1.30)
    LABORAGRIC = st.sidebar.slider('23. Percent of workers in agriculture (Percentage)', 0.00, 1.00, 0.29)
    ANAVGTEMP = st.sidebar.slider('24. Standardized annual mean temperature', 0.00, 1.00, 0.51)
    TEMPANRG = st.sidebar.slider('25. Standardized temperature annual range', 0.00, 1.00, 0.38)
    PRECSEASON = st.sidebar.slider('26. Standardized precipitation seasonality', 0.00, 1.00, 0.54)
    ANPRECIP = st.sidebar.slider('27. Standardized annual precipitation', 0.00, 1.00, 0.25)
    data = {"intercept": 0.943,
            'COCINA_1': COCINA_1,
            'COMBUSTIBLE_1': COMBUSTIBLE_1,
            'ELECTRICIDAD_5': ELECTRICIDAD_5,
            'ABA_AGUA_ENTU_1': ABA_AGUA_ENTU_1,
            'AUTOPROP_7': AUTOPROP_7,
            'CELULAR_3': CELULAR_3,
            'TENENCIA_1': TENENCIA_1,
            'TENENCIA_2': TENENCIA_2,
            'DEUDA_1': DEUDA_1,
            'DEUDA_2': DEUDA_2,
            'INGR_PEROTROPAIS_1': INGR_PEROTROPAIS_1,
            'INGR_PERDENTPAIS_3': INGR_PERDENTPAIS_3,
            'INGR_AYUGOB_5': INGR_AYUGOB_5,
            'TERRENO_AGROPE_1': TERRENO_AGROPE_1,
            'CONACT_10': CONACT_10,
            'CONACT_13': CONACT_13,
            'CONACT_31': CONACT_31,
            'INSAN': INSAN,
            'ALFABET_5_J': ALFABET_5_J,
            'SERSALUD_NO': SERSALUD_NO,
            'EDAD_0_5': EDAD_0_5,
            'EDAD_30_59': EDAD_30_59,
            'LABORAGRIC': LABORAGRIC,
            'ANAVGTEMP': ANAVGTEMP,
            'TEMPANRG': TEMPANRG,
            'PRECSEASON': PRECSEASON,
            'ANPRECIP': ANPRECIP}
    features = pd.DataFrame(data, index= [0])
    return features

df = user_input_features()

# Print specified input parameters
st.header('Specified input parameters')
st.write(df.T)
st.write('---')

######################
# Pre-built model
######################

# Reads in saved model
load_model = pickle.load(open('cc&migration_model.pkl', 'rb'))

# Apply model to make predictions
prediction = load_model.predict(df)

st.header('Predicted probability that the household has at least one migrant: ')
st.write(prediction)